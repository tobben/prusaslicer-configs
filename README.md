# PrusaSlicer-configs

PrusaSlicer-configs for my Hangprinter v4.
These are meant as a starting point for Hangprinter v4 users.

I may not update this repo every time that I change a slicer setting, but I'll update it if I feel I've improved something important.

